<!-- manpage.xsl: various fixups to docbook -> manpage output
  -
  - For now converts it adds a newline after preformatted text enclosed
  - in screen.
  -
  -->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:template match="screen">
	<xsl:text>.sp&#10;.nf&#10;</xsl:text>
	<xsl:apply-templates/>
	<xsl:text>&#10;.fi&#10;.sp&#10;</xsl:text>
</xsl:template>
</xsl:stylesheet>
